#!/usr/bin/env python

import random
import sys
import time
import json
import rospy
from geometry_msgs.msg import Twist
import Adafruit_BBIO.PWM as PWM


SERVO_PIN = "P9_14"
LINEAR_PIN = "P9_16"

def interp(x, x0, x1, y0, y1):
    return (y0*(x1-x)+y1*(x-x0))/(x1-x0)


class Motor(object):
    def __init__(self, pin):
        self.pin = pin
        self.freq = 50 #Hz
        self.started = False


class Linear(Motor):
    def __init__(self, pin):
        super(Linear, self).__init__(pin)
        self.reverse_duty_max = 4
        self.reverse_duty_min = 6.8
        self.stop_duty = 7.3
        self.forward_duty_min = 7.8
        self.forward_duty_max = 9
    
    def start(self):
        if not self.started:
            print("Starting motor")
            PWM.start(self.pin, self.stop_duty, self.freq)
    
    def stop(self):
        print("Stopping motor")
        self.started = False
        PWM.stop(self.pin)
        PWM.cleanup()
    
    def set_speed(self, speed):
        self.start()
        speed = interp(speed, -1, 1, self.forward_duty_max, self.reverse_duty_max)
        print("Speed: {}".format(speed))
        PWM.set_duty_cycle(self.pin, speed)


class Servo(Motor):
    def __init__(self, pin):
        super(Servo, self).__init__(pin)
        self.left_duty = 4.5
        self.center_duty = 7.3
        self.right_duty = 10.0


class RealServo(Servo):
    def start(self):
        if not self.started:
            PWM.start(self.pin, self.center_duty, self.freq)
    
    def stop(self):
        print("Stopping motor")
        self.started = False
        PWM.stop(self.pin)
        PWM.cleanup()

    # angle [-1,1]
    def set_angle(self, angle):
        self.start()
        angle = interp(angle, -1, 1, self.left_duty, self.right_duty)
        print("Angle: {}".format(angle))
        PWM.set_duty_cycle(self.pin, angle)
    
    def set_state(self, linear, angular):
        self.start()
        pwm_linear = interp(linear, -1, 1, self.reverse_duty, self.forward_duty)
        pwm_angular = interp(angular, -1, 1, self.left_duty, self.right_duty)
        print("Linear PWM: {}  Angular PWM: {}".format(pwm_linear, pwm_angular))
        PWM.set_duty_cycle(self.pin, pwm_angular)


class DummyServo(Servo):
    def start(self):
        print("START")

    def stop(self):
        print("STOP")

    def set_angle(self, angle):
        duty = interp(angle, -1.0, 1, self.left_duty, self.right_duty)
        print("{} -> {}".format(angle, duty))
    
    def set_state(self, linear, angular):
        pwm_linear = interp(linear, -1, 1, self.reverse_duty, self.forward_duty)
        pwm_angular = interp(angular, -1, 1, self.left_duty, self.right_duty)
        print("Linear PWM: {}  Angular PWM: {}".format(pwm_linear, pwm_angular))


#servo = DummyServo()
servo = RealServo(SERVO_PIN)
linear = Linear(LINEAR_PIN)

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard linear: %s", data.linear)
    rospy.loginfo(rospy.get_caller_id() + "I heard angular: %s", data.angular)
    #servo.set_state(data.linear.x, data.angular.z)
    servo.set_angle(data.angular.z)
    linear.set_speed(data.linear.x)

def listener():
    rospy.loginfo(rospy.get_caller_id() + "Driver startup")
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'driver' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('driver', anonymous=True)

    rospy.Subscriber("/cmd_vel", Twist, callback)

    # spin() simply keeps python from exiting until this node is stopped
    print("spin")
    rospy.spin()


if __name__ == '__main__':
    listener()
